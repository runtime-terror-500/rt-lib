/* eslint no-new: 0 */
import Component from '../lib/component';

test('Component constructor - misspelled key', () => {
  const badKey = { badKey: { type: 'component', paretn: 'CompC' } };
  expect(() => {
    new Component(badKey);
  }).toThrow('Component: badKey has an invalid key: paretn');
});

test('Component constructor - empty children', () => {
  const noChild = { noChild: { type: 'component', children: null } };
  expect(() => {
    new Component(noChild);
  }).toThrow('noChild has children specified in an invalid manner');
});
