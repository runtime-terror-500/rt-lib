import Graph from '../lib/graph';
import Component from '../lib/component';
import Relation from '../lib/relation';
import UseCase from '../lib/useCase';

const compA = {
  compA: {
    type: 'component',
    description: '#1',
    relationships: ['relA'],
  },
};

const compB = {
  compA: {
    type: 'component',
    description: '#2',
  },
};

const compD = {
  compD: {
    type: 'component',
    relationships: ['relB'],
    description: '#3',
  },
};

const compE = {
  compE: {
    type: 'component',
    children: ['compF'],
    parent: 'compF',
  },
};

const compF = {
  compF: {
    type: 'component',
    children: ['compE'],
    parent: 'compE',
  },
};

const compG = {
  compG: {
    type: 'component',
    children: ['compH'],
  },
};

const compH = {
  compH: {
    type: 'component',
    parent: 'compG',
    relationships: ['asdf'],
  },
};

const compI = {
  compI: {
    type: 'component',
    relationships: ['asdf', 'qwer'],
  },
};

const relA = {
  relA: {
    type: 'relation',
    description: '#1',
    from: 'compD',
    to: 'compD',
  },
};

const relB = {
  relA: {
    type: 'relation',
    description: '#2',
    from: 'compD',
    to: 'compA',
  },
};

const useCaseA = {
  useCaseA: {
    type: 'use case',
    path: ['ucCompA', 'ucRelA', 'ucCompB'],
  },
};

const useCaseB = {
  useCaseB: {
    type: 'use case',
    path: ['ucCompA', 'ucCompB'],
  },
};

const useCaseC = {
  useCaseC: {
    type: 'use case',
    path: ['ucRelA', 'ucRelB'],
  },
};

const ucCompA = {
  ucCompA: {
    type: 'component',
    relationships: ['ucRelA', 'ucRelB'],
  },
};

const ucCompB = {
  ucCompB: {
    type: 'component',
    relationships: ['ucRelA', 'ucRelB'],
  },
};

const ucRelA = {
  ucRelA: {
    type: 'relationship',
    from: 'ucCompA',
    to: 'ucCompB',
  },
};

const ucRelB = {
  ucRelB: {
    type: 'relationship',
    from: 'ucCompB',
    to: 'ucCompA',
  },
};

const nestCompA = {
  nestCompA: {
    type: 'component',
  },
};

const nestCompB = {
  nestCompB: {
    type: 'component',
    children: ['nestCompA'],
  },
};

const nestCompD = {
  nestCompD: {
    type: 'component',
    parent: 'nestCompE',
    relationships: ['nestRelationA'],
  },
};

const nestCompE = {
  nestCompE: {
    type: 'component',
    children: ['nestCompD'],
  },
};

const nestCompF = {
  nestCompF: {
    type: 'component',
    relationships: ['nestRelationA'],
  },
};

const nestRelationA = {
  nestRelationA: {
    type: 'relationship',
    from: 'nestCompD',
    to: 'nestCompF',
  },
};

const reflexiveComp = {
  reflexiveComp: {
    type: 'component',
    relationships: ['reflexiveRel'],
  },
};

const reflexiveRel = {
  reflexiveRel: {
    type: 'relationship',
    from: 'reflexiveComp',
    to: 'reflexiveComp',
  },
};

const subGraphCompParent = {
  subGraphCompParent: {
    type: 'component',
    children: ['subGraphCompChildA', 'subGraphCompChildB'],
  },
};

const subGraphCompChildA = {
  subGraphCompChildA: {
    type: 'component',
    parent: 'subGraphCompParent',
  },
};

const subGraphCompChildB = {
  subGraphCompChildB: {
    type: 'component',
    parent: 'subGraphCompParent',
  },
};

const subGraphRel = {
  subGraphRel: {
    type: 'relationship',
    from: 'subGraphCompChildA',
    to: 'subGraphCompChildB',
  },
};

const subGraphUC = {
  subGraphUC: {
    type: 'use case',
    path: ['subGraphCompChildA', 'subGraphRel', 'subGraphCompChildB'],
  },
};

test('Test Constructor', () => {
  const test = new Graph([], [], []);
  expect(test.relationships).toEqual([]);
  expect(test.components).toEqual([]);
});

test('Test Unique component names', () => {
  const test = new Graph([new Component(compA), new Component(compB)], [], []);
  const errors = test.validate();
  const expectedMessage = 'One or more components has the same name:\n'
  + `  -${Object.keys(compA)[0]}\n`
  + `  -${Object.keys(compB)[0]}\n`;
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

test('Test Cyclic dependancy check', () => {
  const test = new Graph([new Component(compE), new Component(compF)], [], []);
  const errors = test.validate();
  const expectedMessage = 'One or more cyclic dependencies exist in the components children\n'
  + `  -${Object.keys(compE)[0]}\n`
  + `  -${Object.keys(compF)[0]}\n`;
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

test('Test Unique relation names', () => {
  const test = new Graph([], [new Relation(relA), new Relation(relB)], []);
  const errors = test.validate();
  const expectedMessage = 'One or more relations has the same name:\n'
  + `  -${Object.keys(relA)[0]}\n`
  + `  -${Object.keys(relB)[0]}\n`;
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

test('Test relations refer to existing components', () => {
  const test = new Graph([], [new Relation(relA)], []);
  const errors = test.validate();
  const expectedMessage = 'One or more relations refer to components that don\'t exist:\n'
  + `  -${Object.keys(relA)[0]}\n`;
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

test('Test relations arent reflexive', () => {
  const test = new Graph([new Component(reflexiveComp)], [new Relation(reflexiveRel)], []);
  const errors = test.validate();
  const expectedMessage = 'One or more relations is reflexive:\n'
  + `  -${Object.keys(reflexiveRel)[0]}\n`;
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

test('Test valid references to components in relations', () => {
  const test = new Graph([new Component(compD)], [new Relation(relA)], []);
  const errors = test.validate();
  const expectedMessage = 'One or more relations refer to a component that does not have it in its relations attribute:\n'
  + `  -${Object.keys(relA)[0]}\n`;
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

test('Test components children refer to existing components', () => {
  const test = new Graph([new Component(compG)], [], []);
  const errors = test.validate();
  const expectedMessage = 'One or more components refer to children that don\'t exist\n'
                           + `  -"${Object.keys(compG)[0]}" has the following invalid nodes:\n`
                           + '    -compH\n';
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

test('Test child with non-existent relationship', () => {
  const test = new Graph([new Component(compG), new Component(compH)],
    [], []);
  const errors = test.validate();
  const expectedMessage = 'One or more child components have non-existent relationships\n'
                           + `  -"${Object.keys(compH)[0]}" has the following invalid relationships:\n`
                           + '    -asdf\n';
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

test('Test Component with non-existent relationship', () => {
  const test = new Graph([new Component(compI)], [], []);
  const errors = test.validate();
  const expectedMessage = 'One or more components have non-existent relationships:\n'
                           + `  -"${Object.keys(compI)[0]}" has the following invalid relationships:\n`
                           + '    -asdf\n'
                           + '    -qwer\n';
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

test('validUseCasePaths - valid', () => {
  const testGraph = new Graph(
    [new Component(ucCompA), new Component(ucCompB)],
    [new Relation(ucRelA), new Relation(ucRelB)],
    [new UseCase(useCaseA)],
  );
  const errors = testGraph.validate();
  expect(errors.length).toBe(0);
});

// This tests a path in which the entities stated in the path do not exist
test('validUseCasePaths - non-existant entity', () => {
  const testGraph = new Graph(
    [],
    [],
    [new UseCase(useCaseA)],
  );
  const errors = testGraph.validate();
  const expectedMessage = 'One or more use cases have invalid paths:\n'
                           + `  -"${Object.keys(useCaseA)[0]}" has the following invalid nodes:\n`
                           + `    -${Object.keys(ucCompA)[0]}\n`
                           + `    -${Object.keys(ucRelA)[0]}\n`
                           + `    -${Object.keys(ucCompB)[0]}\n`;
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

// This tests a path in which two components are listed sequentially
test('validUseCasePaths - component to component', () => {
  const testGraph = new Graph(
    [new Component(ucCompA), new Component(ucCompB)],
    [],
    [new UseCase(useCaseB)],
  );
  const errors = testGraph.validate();
  const expectedMessage = 'One or more use cases have invalid paths:\n'
                           + `  -"${Object.keys(useCaseB)[0]}" has the following invalid nodes:\n`
                           + `    -${Object.keys(ucCompB)[0]}\n`;
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

// This tests a path in which two relationships are listed sequentially
test('validUseCasePaths - relation to relation', () => {
  const testGraph = new Graph(
    [new Component(ucCompA), new Component(ucCompB)],
    [new Relation(ucRelA), new Relation(ucRelB)],
    [new UseCase(useCaseC)],
  );
  const errors = testGraph.validate();
  const expectedMessage = 'One or more use cases have invalid paths:\n'
                           + `  -"${Object.keys(useCaseC)[0]}" has the following invalid nodes:\n`
                           + `    -${Object.keys(ucRelA)[0]}\n`
                           + `    -${Object.keys(ucRelB)[0]}\n`;
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

// TODO I think this tests the same as the one above check with nick
// This tests a path in which two relationships are listed back to back
test('validUseCasePaths - component to wrong relation', () => {
  const testGraph = new Graph(
    [new Component(ucCompA), new Component(ucCompB)],
    [new Relation(ucRelB)],
    [new UseCase(useCaseC)],
  );
  const errors = testGraph.validate();
  const expectedMessage = 'One or more use cases have invalid paths:\n'
                           + `  -"${Object.keys(useCaseC)[0]}" has the following invalid nodes:\n`
                           + `    -${Object.keys(ucRelA)[0]}\n`
                           + `    -${Object.keys(ucRelB)[0]}\n`;
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

// This tests a path in which the relationship stated in the path does
// not exist.
test('validUseCasePaths - relation to wrong component', () => {
  const testGraph = new Graph(
    [new Component(ucCompA), new Component(ucCompB)],
    [new Relation(ucRelB)],
    [new UseCase(useCaseA)],
  );
  const errors = testGraph.validate();
  const expectedMessage = 'One or more use cases have invalid paths:\n'
                           + `  -"${Object.keys(useCaseA)[0]}" has the following invalid nodes:\n`
                           + `    -${Object.keys(ucRelA)[0]}\n`
                           + `    -${Object.keys(ucCompB)[0]}\n`;
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

test('validChildReferences - child parent dont match', () => {
  const testGraph = new Graph(
    [new Component(nestCompA), new Component(nestCompB)],
    [],
    [],
  );
  const errors = testGraph.validate();
  const expectedMessage = 'One or more parents refer to children that don\'t have them as their parent\n'
  + `  -"${Object.keys(nestCompB)[0]}" has the following invalid children:\n`
  + `    -${Object.keys(nestCompA)[0]}\n`;
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

test('validChildrenRelations - child relates to non-sibling', () => {
  const testGraph = new Graph([new Component(nestCompD),
    new Component(nestCompE),
    new Component(nestCompF)],
  [new Relation(nestRelationA)],
  []);
  const errors = testGraph.validate();
  const expectedMessage = 'One or more parent components have children that have relationships outside the parent\n'
  + `  -"${Object.keys(nestCompE)[0]}" has the following children in violation:\n`
  + `    -"${Object.keys(nestCompD)[0]}" has the following invalid references:\n`
  + `      -${Object.keys(nestRelationA)[0]}\n`;
  expect(errors.some((error) => error.message === expectedMessage)).toBeTruthy();
});

test('build sub graphs', () => {
  const testGraph = new Graph([new Component(subGraphCompParent),
    new Component(subGraphCompChildA),
    new Component(subGraphCompChildB)],
  [new Relation(subGraphRel)],
  [new UseCase(subGraphUC)]);

  testGraph.buildSubGraphs();

  expect(testGraph.components[0].subGraph.components.length === 2).toBeTruthy();
  expect(testGraph.components[0].subGraph.relationships.length === 1).toBeTruthy();
  expect(testGraph.components[0].subGraph.useCases.length === 1).toBeTruthy();
  expect(testGraph.components.length === 1).toBeTruthy();
  expect(testGraph.relationships.length === 0).toBeTruthy();
  expect(testGraph.useCases.length === 0).toBeTruthy();
});
