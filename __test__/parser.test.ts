/* eslint no-new: 0 */
import * as path from 'path';
import * as fs from 'fs';
import { YAMLException } from 'js-yaml';
import Parser from '../lib/parser';
import YAMLSpecError from '../lib/YAMLSpecError';

const validYaml = fs.readFileSync(path.join(__dirname, 'fixtures', 'TestYAML.yaml'), 'utf8');
const invalidYaml = fs.readFileSync(path.join(__dirname, 'fixtures', 'TestBadYAML.yaml'), 'utf8');

test('buildGraphEntities - valid YAML', () => {
  const graphEntities = Parser.buildGraphEntities(validYaml);
  expect(graphEntities.components.length).toBe(1);
  expect(graphEntities.components[0].name).toBe('Component A');
  expect(graphEntities.relationships.length).toBe(1);
  expect(graphEntities.relationships[0].name).toBe('Filbert Flange Exchange');
  expect(graphEntities.useCases.length).toBe(1);
  expect(graphEntities.useCases[0].name).toBe('Export Filbert Flanges');
});

test('buildGraphEntites - invalid YAML', () => {
  try {
    Parser.buildGraphEntities(invalidYaml);
  } catch (e) {
    expect(e instanceof YAMLException).toBeTruthy();
  }
});

test('buildGraphEntities - invalid entity type', () => {
  expect(() => {
    Parser.buildGraphEntities(path.join(__dirname, 'fixtures', 'invalidEntityType.yaml'));
  }).toThrowError(YAMLSpecError);
});

test.todo('buildGraphEntities - invalid entity format');
