/* eslint no-new: 0 */
import Relation from '../lib/relation';

test('Relation constructor - bad key', () => {
  const badKey = { badKey: { type: 'relationship', to: 'compA', form: 'compB' } };
  expect(() => {
    new Relation(badKey);
  }).toThrow('Relationship: badKey has an invalid key: form');
});
