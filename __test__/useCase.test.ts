/* eslint no-new: 0 */
import * as fs from 'fs';
import * as yaml from 'js-yaml';
import * as path from 'path';
import UseCase from '../lib/useCase';

let useCases;

beforeAll(() => {
  const file = path.join(__dirname, 'fixtures/UseCaseTest.yaml');
  const fileContent = fs.readFileSync(file, 'utf8');
  const useCasesObject = yaml.safeLoad(fileContent);
  useCases = Object.keys(useCasesObject).map((i) => {
    const entityObject = {};
    entityObject[i] = useCasesObject[i];
    return entityObject;
  });
});

test('UseCase Constructor - valid', () => {
  const validUseCaseObject = useCases[0];
  const useCase = new UseCase(validUseCaseObject);
  expect(useCase.name).toBe('Export Filbert Flanges');
  expect(useCase.description).toBe('send filbert flanges from Component A to Component D using the Filbert Flange Exchange');
  expect(useCase.path).toEqual(['Component A', 'Filbert Flange Exchange', 'Component D']);
  expect(useCase.metadata).toEqual({ artifact: 'Filbert Flange', actors: ['Bill', 'Ted'] });
});

test('UseCase Constructor - invalid path', () => {
  const invalidPathUseCaseObject = useCases[1];
  expect(() => {
    new UseCase(invalidPathUseCaseObject);
  }).toThrow();
});

test('UseCase Constructor - missing path', () => {
  const missingPathUseCaseObject = useCases[2];
  expect(() => {
    new UseCase(missingPathUseCaseObject);
  }).toThrow('The Use Case "Missing Path Use Case" contains an invalid path');
});

test('UseCase Constructor - empty path', () => {
  const emptyPathUseCaseObject = useCases[3];
  expect(() => {
    new UseCase(emptyPathUseCaseObject);
  }).toThrow('The Use Case "Empty Path Use Case" contains an invalid path');
});

test('UseCase Constructor - invalid type', () => {
  const invalidTypeUseCaseObject = useCases[4];
  expect(() => {
    new UseCase(invalidTypeUseCaseObject);
  }).toThrow('Usecase constructor received aninvalid entity type. Received "not use case" expected "use case"');
});

test('UseCase Constructor - missing type', () => {
  const missingTypeUseCaseObject = useCases[5];
  expect(() => {
    new UseCase(missingTypeUseCaseObject);
  }).toThrow('Usecase constructor received aninvalid entity type. Received "undefined" expected "use case"');
});

test('UseCase Constructor - misspelled key', () => {
  const badkeyUseCase = { badKey: { type: 'use case', paht: ['a', 'b'] } };
  expect(() => {
    new UseCase(badkeyUseCase);
  }).toThrow('Use case: badKey has an invalid key: paht');
});
