import * as _ from 'lodash';
import YAMLSpecError from './YAMLSpecError';
import Graph from './graph';

export default class Component {
  name: string;

  description: string = ''; // Optional

  children: Array<string> = []; // Optional

  parent: string = ''; // Optional

  relationships: Array<string> = []; // Optional

  metadata: Object; // Optional

  subGraph: Graph;

  constructor(obj: object) {
    const keys = Object.keys(obj);
    if (keys.length !== 1) {
      throw new YAMLSpecError('Object must be in the form:'
                              + '{name: {Component data}'
                              + `Got the following:\n${obj}`);
    }
    [this.name] = keys;
    Object.keys(obj[this.name]).forEach((key) => {
      switch (key) {
        case 'description': {
          this.description = obj[this.name].description;
          break;
        }
        case 'relationships': {
          if (_.isArray(obj[this.name].relationships)) {
            this.relationships = obj[this.name].relationships;
          } else {
            throw new YAMLSpecError(`Component: ${this.name} has relationships specified in a invalid manner`);
          }
          break;
        }
        case 'children': {
          if (_.isArray(obj[this.name].children)) {
            this.children = obj[this.name].children;
          } else {
            throw new YAMLSpecError(`Component: ${this.name} has children specified in an invalid manner`);
          }
          break;
        }
        case 'parent': {
          this.parent = obj[this.name].parent;
          break;
        }
        case 'metadata': {
          this.metadata = obj[this.name].metadata;
          break;
        }
        case 'type': {
          // Do nothing, as these fields don't belong in the component itself
          break;
        }
        default: {
          throw new YAMLSpecError(`Component: ${this.name} has an invalid key: ${key}`);
        }
      }
    });
  }
}
