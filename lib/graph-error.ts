export default class GraphError {
  line?: number; // OPTIONAL

  message: string;

  constructor(message: string, line?: number) {
    this.line = line;
    this.message = message;
  }
}
