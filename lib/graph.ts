import { YAMLException } from 'js-yaml';
import Parser from './parser';
import Component from './component';
import Relation from './relation';
import UseCase from './useCase';
import GraphError from './graph-error';
import YAMLSpecError from './YAMLSpecError';


export default class Graph {
  components: Array<Component>;

  relationships: Array<Relation>;

  useCases: Array<UseCase>;

  constructor(
    components: Array<Component>,
    relationships: Array<Relation>,
    useCases: Array<UseCase>,
  ) {
    this.components = components;
    this.relationships = relationships;
    this.useCases = useCases;
  }

  static build(rawYaml: string): Promise<Graph> {
    return new Promise((resolve, reject) => {
      try {
        const graphEntities = Parser.buildGraphEntities(rawYaml);
        const graph = new Graph(
          graphEntities.components,
          graphEntities.relationships,
          graphEntities.useCases,
        );

        const errors: Array<GraphError> = graph.validate();
        if (errors.length !== 0) {
          reject(errors);
        }

        graph.buildSubGraphs();
        resolve(graph);
      } catch (error) {
        if (error instanceof YAMLException) {
          const e: Array<GraphError> = [new GraphError(`Invalid YAML: ${error.message}`)];
          reject(e);
        } else if (error instanceof YAMLSpecError) {
          const e: Array<GraphError> = [new GraphError(`Improperly specified YAML: ${error.message}`)];
          reject(e);
        } else {
          throw new Error(`Encountered unhandled exception while building graph: ${error.message}\n stacktrace: ${error.stacktrace}`);
        }
      }
    });
  }

  findComponentByName(name: string): Component {
    return this.components.find((component) => component.name === name);
  }

  findRelationByName(name: string): Relation {
    return this.relationships.find((relation) => relation.name === name);
  }

  findEntityByName(name: string): Component | Relation | UseCase {
    const component = this.components.find((c) => c.name === name);
    const relation = this.relationships.find((r) => r.name === name);
    const useCase = this.components.find((u) => u.name === name);

    return component || relation || useCase;
  }

  validate(): Array<GraphError> {
    const errors: Array<GraphError> = [];

    // Validate unique Components
    let dups = this.uniqueComponentNames();
    if (dups.length !== 0) {
      let message = 'One or more components has the same name:\n';
      dups.forEach((element) => {
        message += `  -${element}\n`;
      });
      errors.push(new GraphError(message));
    }

    // Validate unique Relations
    dups = this.uniqueRelationNames();
    if (dups.length !== 0) {
      let message = 'One or more relations has the same name:\n';
      dups.forEach((element) => {
        message += `  -${element}\n`;
      });
      errors.push(new GraphError(message));
    }

    // Validate that all relationships exist
    const badRelList = this.validComponentRelationships();
    if (Object.keys(badRelList).length !== 0) {
      let message = 'One or more components have non-existent relationships:\n';
      const badRelKeys = Object.keys(badRelList);
      for (let i = 0; i < badRelKeys.length; i += 1) {
        if (Object.prototype.hasOwnProperty.call(badRelList, badRelKeys[i])) {
          message += `  -"${badRelKeys[i]}" has the following invalid relationships:\n`;
          const modifiedList: String[] = badRelList[badRelKeys[i]].map((element) => `    -${element}\n`);
          message += modifiedList.join('');
        }
      }
      errors.push(new GraphError(message));
    }

    // Validate if the Component that a Relation references exists if
    // they do check that components also reference them
    const badRefs = this.existentRelationReferences();
    if (badRefs.length !== 0) {
      let message = 'One or more relations refer to components that don\'t exist:\n';
      badRefs.forEach((element) => {
        message += `  -${element}\n`;
      });
      errors.push(new GraphError(message));
    } else {
      // Validate if the Component referenced in the Relation references the Relation
      const notValid = this.validRelationReferences();
      if (notValid.length !== 0) {
        let message = 'One or more relations refer to a component that does not have it in its relations attribute:\n';
        notValid.forEach((element) => {
          message += `  -${element}\n`;
        });
        errors.push(new GraphError(message));
      }
    }

    // Validate that all no relation has a single component as both 'to' and 'from'
    const reflexiveRelations = this.nonReflexiveRelations();
    if (reflexiveRelations.length !== 0) {
      let message = 'One or more relations is reflexive:\n';
      reflexiveRelations.forEach((element) => {
        message += `  -${element}\n`;
      });
      errors.push(new GraphError(message));
    }

    // Validate all components and relationships within a path are valid
    const badPaths = this.validUseCasePaths();
    if (Object.keys(badPaths).length !== 0) {
      let message = 'One or more use cases have invalid paths:\n';
      const badPathKeys = Object.keys(badPaths);
      for (let i = 0; i < badPathKeys.length; i += 1) {
        if (Object.prototype.hasOwnProperty.call(badPaths, badPathKeys[i])) {
          message += `  -"${badPathKeys[i]}" has the following invalid nodes:\n`;
          const modifiedList: String[] = badPaths[badPathKeys[i]].map((element) => `    -${element}\n`);
          message += modifiedList.join('');
        }
      }
      errors.push(new GraphError(message));
    }

    // Validate if the children referenced exist if they all do
    // check the children for errors
    const nonExistentChildren = this.existentChildrenReferences();
    if (Object.keys(nonExistentChildren).length !== 0) {
      let message = 'One or more components refer to children that don\'t exist\n';
      const nonExistKeys = Object.keys(nonExistentChildren);
      for (let i = 0; i < nonExistKeys.length; i += 1) {
        if (Object.prototype.hasOwnProperty.call(nonExistentChildren, nonExistKeys[i])) {
          message += `  -"${nonExistKeys[i]}" has the following invalid nodes:\n`;
          const modifiedList: String[] = nonExistentChildren[nonExistKeys[i]].map((element) => `    -${element}\n`);
          message += modifiedList.join('');
        }
      }
      errors.push(new GraphError(message));
    } else {
      // Check to see if children do not reference the component as a parent
      const badChildren = this.validChildrenReferences();
      if (Object.keys(badChildren).length !== 0) {
        let message = 'One or more parents refer to children that don\'t have them as their parent\n';
        const badChildrenKeys = Object.keys(badChildren);
        for (let i = 0; i < badChildrenKeys.length; i += 1) {
          if (Object.prototype.hasOwnProperty.call(badChildren, badChildrenKeys[i])) {
            message += `  -"${badChildrenKeys[i]}" has the following invalid children:\n`;
            const modifiedList: String[] = badChildren[badChildrenKeys[i]].map((element) => `    -${element}\n`);
            message += modifiedList.join('');
          }
        }
        errors.push(new GraphError(message));
      }
      // Validate if there is any cyclical dependencies
      const cyclicDependencies = this.nonCyclicalChildren();
      if (cyclicDependencies.length !== 0) {
        let message = 'One or more cyclic dependencies exist in the components children\n';
        cyclicDependencies.forEach((element) => {
          message += `  -${element}\n`;
        });
        errors.push(new GraphError(message));
      }

      // Check if children actually reference relations that exist
      const nonExistentChildrenRelationships = this.validChildrenRelationships();
      if (Object.keys(nonExistentChildrenRelationships).length !== 0) {
        let message = 'One or more child components have non-existent relationships\n';
        const badChildrenKeys = Object.keys(nonExistentChildrenRelationships);
        for (let i = 0; i < badChildrenKeys.length; i += 1) {
          if (Object.prototype.hasOwnProperty.call(nonExistentChildrenRelationships,
            badChildrenKeys[i])) {
            message += `  -"${badChildrenKeys[i]}" has the following invalid relationships:\n`;
            const modifiedList: String[] = nonExistentChildrenRelationships[badChildrenKeys[i]].map((element) => `    -${element}\n`);
            message += modifiedList.join('');
          }
        }
        errors.push(new GraphError(message));
      } else {
        // Check to see that children only reference other children of their parent
        const badChildrenRefs = this.validSubgraphRelationships();
        if (Object.keys(badChildrenRefs).length !== 0) {
          let message = 'One or more parent components have children that have relationships outside the parent\n';
          const badKeys = Object.keys(badChildrenRefs);
          for (let i = 0; i < badKeys.length; i += 1) {
            if (Object.prototype.hasOwnProperty.call(badChildrenRefs, badKeys[i])) {
              message += `  -"${badKeys[i]}" has the following children in violation:\n`;
              const badChildrenKeys = Object.keys(badChildrenRefs[badKeys[i]]);
              for (let j = 0; j < badChildrenKeys.length; j += 1) {
                if (Object.prototype.hasOwnProperty.call(
                  badChildrenRefs[badKeys[i]], badChildrenKeys[j],
                )) {
                  message += `    -"${badChildrenKeys[j]}" has the following invalid references:\n`;
                  const modifiedList: String[] = badChildrenRefs[badKeys[i]][badChildrenKeys[j]].map((element) => `      -${element}\n`);
                  message += modifiedList.join('');
                }
              }
            }
          }
          errors.push(new GraphError(message));
        }
      }
    }
    return errors;
  }

  private uniqueComponentNames(): String[] {
    const names = this.components.map((component) => component.name);
    if (names.length === new Set(names).size) {
      return [];
    }
    return names.filter((i) => names.filter((ii) => ii === i).length > 1);
  }

  private uniqueRelationNames(): String[] {
    const names = this.relationships.map((relationship) => relationship.name);
    if (names.length === new Set(names).size) {
      return [];
    }
    return names.filter((i) => names.filter((ii) => ii === i).length > 1);
  }

  private uniqueUseCaseNames(): String[] {
    const names = this.useCases.map((useCase) => useCase.name);
    if (names.length === new Set(names).size) {
      return [];
    }
    return names.filter((i) => names.filter((ii) => ii === i).length > 1);
  }

  private validUseCasePaths(): object {
    const nonValid = {};
    this.useCases.forEach((useCase) => {
      let lastPathEntity;
      const badPathEntities = useCase.path.filter((pathEntityName) => {
        const pathEntity = this.findEntityByName(pathEntityName);
        if (pathEntity === undefined) {
          return true;
        }

        let nonValidPathEntity = true;
        if (lastPathEntity === undefined && pathEntity instanceof Component) {
          lastPathEntity = pathEntity;
          nonValidPathEntity = false;
        } else if (lastPathEntity instanceof Component
          && pathEntity instanceof Relation
          && pathEntity.from === lastPathEntity.name) {
          lastPathEntity = pathEntity;
          nonValidPathEntity = false;
        } else if (
          lastPathEntity instanceof Relation
          && pathEntity instanceof Component
          && lastPathEntity.to === pathEntity.name) {
          lastPathEntity = pathEntity;
          nonValidPathEntity = false;
        }

        return nonValidPathEntity;
      });

      if (badPathEntities.length !== 0) {
        nonValid[useCase.name] = badPathEntities;
      }
    });

    return nonValid;
  }

  private nonCyclicalChildren(): String[] {
    // For reference: https://algorithms.tutorialhorizon.com/graph-detect-cycle-in-a-directed-graph/
    const visited: Hash<boolean> = {};
    const recursiveArr: Hash<boolean> = {};
    this.components.forEach((component) => {
      visited[component.name] = false;
      recursiveArr[component.name] = false;
    });

    const cyclic = this.components.filter((component) => !this.nonCycleUtil(component,
      visited,
      recursiveArr).result);

    if (cyclic.length === 0) {
      return [];
    }
    return cyclic.map((element) => element.name);
  }

  private nonCycleUtil(
    vertex: Component,
    visited: Hash<boolean>,
    recursiveArr: Hash<boolean>,
  ): {result: boolean; recursiveArr: Hash<boolean> } {
    const visitedLocal: Hash<boolean> = visited;
    let recursiveArrLocal: Hash<boolean> = recursiveArr;
    visitedLocal[vertex.name] = true;
    recursiveArrLocal[vertex.name] = true;

    const result = vertex.children.every((child) => { // result is false if any cycle exists
      const childComponent: Component = this.findComponentByName(child);
      if (!visitedLocal[child]) {
        const returnedValues = this.nonCycleUtil(childComponent, visitedLocal, recursiveArrLocal);
        if (!returnedValues.result) {
          return false;
        }
        recursiveArrLocal = returnedValues.recursiveArr;
      }
      if (recursiveArrLocal[child]) {
        return false;
      }
      return true;
    });

    if (result === true) {
      recursiveArrLocal[vertex.name] = false;
    }

    return {
      result,
      recursiveArr: recursiveArrLocal,
    };
  }

  private existentRelationReferences(): String[] {
    const badRefs = this.relationships.filter((relation) => {
      const fromComponent = this.findComponentByName(relation.from);
      const toComponent = this.findComponentByName(relation.to);
      return fromComponent === undefined || toComponent === undefined;
    });
    if (badRefs.length === 0) {
      return [];
    }
    return badRefs.map((relation) => relation.name);
  }

  private validRelationReferences(): String[] {
    const nonValid = this.relationships.filter((relation) => {
      const fromComponent = this.findComponentByName(relation.from);
      const toComponent = this.findComponentByName(relation.to);

      return !fromComponent.relationships.includes(relation.name)
          || !toComponent.relationships.includes(relation.name);
    });
    if (nonValid.length === 0) {
      return [];
    }
    return nonValid.map((relation) => relation.name);
  }

  private nonReflexiveRelations(): String[] {
    const reflexive = this.relationships.filter((relation) => relation.to === relation.from);
    if (reflexive.length === 0) {
      return [];
    }
    return reflexive.map((relation) => relation.name);
  }

  private existentChildrenReferences(): Object {
    const invalidObj = {};
    this.components.forEach((component) => {
      if (typeof component.children === 'undefined') {
        return;
      }
      const nonExistent = component.children.filter((child) => {
        const childComponent = this.findComponentByName(child);
        return typeof childComponent === 'undefined';
      });
      if (nonExistent.length !== 0) {
        invalidObj[component.name] = nonExistent;
      }
    });
    return invalidObj;
  }

  private validChildrenReferences(): Object {
    const invalidObj = {};
    this.components.forEach((component) => {
      if (typeof component.children === 'undefined') {
        return;
      }
      const badChildren = component.children.filter((child) => {
        const childComponent = this.findComponentByName(child);
        return !(childComponent.parent === component.name);
      });
      if (badChildren.length !== 0) {
        invalidObj[component.name] = badChildren;
      }
    });
    return invalidObj;
  }

  // Check to see if components have valid relationships
  private validComponentRelationships(): Object {
    const invalidObj = {};
    this.components.forEach((component) => {
      // Exclude checking children components, they are checked in a different spot
      if (component.parent !== '') {
        return;
      }
      const relList = component.relationships.filter((relation) => {
        const relationship = this.findRelationByName(relation);
        return (relationship === undefined);
      });
      if (relList.length !== 0) {
        invalidObj[component.name] = relList;
      }
    });
    return invalidObj;
  }

  // Check to see if all the relationships in a child component actually exist
  private validChildrenRelationships(): Object {
    const invalidObj = {};
    this.components.forEach((component) => {
      if (typeof component.children === 'undefined') {
        return;
      }
      component.children.forEach((child) => {
        const childComponent = this.findComponentByName(child);
        if (typeof childComponent.relationships === 'undefined') {
          return;
        }
        const relList = childComponent.relationships.filter((childRelationName) => {
          const childRelation = this.findRelationByName(childRelationName);
          return (childRelation === undefined);
        });
        if (relList.length !== 0) {
          invalidObj[childComponent.name] = relList;
        }
      });
    });
    return invalidObj;
  }

  // Only allow children to relate to other chldren of the same parent for graph rendering purposes
  private validSubgraphRelationships(): Object {
    const invalidObj = {};
    this.components.forEach((component) => {
      if (typeof component.children === 'undefined') {
        return;
      }
      const badChildren = {};
      component.children.forEach((child) => {
        const childComponent = this.findComponentByName(child);
        if (typeof childComponent.relationships === 'undefined') {
          return;
        }
        const relList = childComponent.relationships.filter((childRelationName) => {
          const childRelation = this.findRelationByName(childRelationName);
          return !(component.children.includes(childRelation.to)
            && component.children.includes(childRelation.from));
        });
        if (relList.length !== 0) {
          badChildren[childComponent.name] = relList;
        }
      });
      if (Object.keys(badChildren).length !== 0) {
        invalidObj[component.name] = badChildren;
      }
    });
    return invalidObj;
  }

  /* eslint no-param-reassign: ["error", { "props": false }] */
  buildSubGraphs(): void {
    let totalSubComponents: Array<Component> = [];
    let totalSubRelations: Array<Relation> = [];
    let totalSubUseCases: Array<UseCase> = [];
    this.components.forEach((component) => {
      if (component.children.length > 0) {
        const subComponents: Array<Component> = component.children.map(
          (compName) => this.findComponentByName(compName),
        );
        const subRelations: Array<Relation> = this.relationships.filter(
          (relation) => component.children.includes(relation.from)
          && component.children.includes(relation.to),
        );
        const subUseCases: Array<UseCase> = this.useCases.filter(
          (useCase) => useCase.path.every(
            (element) => component.children.includes(element)
            || subRelations.includes(this.findRelationByName(element)),
          ),
        );
        component.subGraph = new Graph(subComponents, subRelations, subUseCases);
        totalSubComponents = totalSubComponents.concat(subComponents);
        totalSubRelations = totalSubRelations.concat(subRelations);
        totalSubUseCases = totalSubUseCases.concat(subUseCases);
      }
    });

    // Remove items from the top level graph that appear in a subgraph
    this.components = this.components.filter(
      (component) => !totalSubComponents.includes(component),
    );
    this.relationships = this.relationships.filter(
      (relation) => !totalSubRelations.includes(relation),
    );
    this.useCases = this.useCases.filter(
      (useCase) => !totalSubUseCases.includes(useCase),
    );
  }
}

interface Hash<T> { [key: string]: T; }
