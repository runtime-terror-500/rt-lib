import * as yaml from 'js-yaml';
import Relation from './relation';
import Component from './component';
import UseCase from './useCase';
import YAMLSpecError from './YAMLSpecError';

interface GraphData {
  components: Array<Component>,
  relationships: Array<Relation>,
  useCases: Array<UseCase>
}

export default class Parser {
  private static readYaml(rawYaml: string): Array<Object> {
    // TODO have to have more robust path checking
    // TODO validate initial YAML structure
    const graphData = yaml.safeLoad(rawYaml);
    const graphEntities = Object.keys(graphData).map((i) => {
      const entityObject = {};
      entityObject[i] = graphData[i];
      return entityObject;
    });

    return graphEntities;
  }

  static buildGraphEntities(rawYaml: string): GraphData {
    const entities = Parser.readYaml(rawYaml);
    const components = [];
    const relationships = [];
    const useCases = [];

    entities.forEach((entity) => {
      try {
        const entityType = entity[Object.getOwnPropertyNames(entity)[0]].type;
        switch (entityType) {
          case 'component': {
            components.push(new Component(entity));
            break;
          }
          case 'relationship': {
            relationships.push(new Relation(entity));
            break;
          }
          case 'use case': {
            useCases.push(new UseCase(entity));
            break;
          }
          default: {
            const keys = Object.keys(entity);
            let entityName;
            if (Object.prototype.hasOwnProperty.call(entity, keys[0])) {
              [entityName] = keys;
            }
            throw new YAMLSpecError(
              `"${entityName}" has an invalid entity type: ${entityType}`,
            );
          }
        }
      } catch (e) {
        if (e instanceof YAMLSpecError) {
          throw e;
        } else {
          throw new Error(`Encountered unknown exception while parsing graph entities:\n ${e.message}`);
        }
      }
    });

    return { components, relationships, useCases };
  }
}
