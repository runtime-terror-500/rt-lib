import YAMLSpecError from './YAMLSpecError';

export default class Relation {
  name: string;

  description: string = ''; // Optional

  from: string;

  to: string;

  metadata: Object; // Optional

  constructor(obj: object) {
    const keys = Object.keys(obj);
    if (keys.length !== 1) {
      throw new YAMLSpecError('Object must be in the form:'
                              + '{name: {Relationship data}'
                              + `Got the following: \n${obj}`);
    }
    [this.name] = keys;
    Object.keys(obj[this.name]).forEach((key) => {
      switch (key) {
        case 'to': {
          this.to = obj[this.name].to;
          break;
        }
        case 'from': {
          this.from = obj[this.name].from;
          break;
        }
        case 'description': {
          this.description = obj[this.name].description;
          break;
        }
        case 'metadata': {
          this.metadata = obj[this.name].metadata;
          break;
        }
        case 'type': {
          break;
        }
        default: {
          throw new YAMLSpecError(`Relationship: ${this.name} has an invalid key: ${key}`);
        }
      }
    });
  }
}
