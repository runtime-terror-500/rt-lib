import * as _ from 'lodash';
import YAMLSpecError from './YAMLSpecError';

export default class UseCase {
  name: string;

  description: string;

  path: Array<string>;

  metadata: Object;

  constructor(rawUseCase: object) {
    const keys = Object.keys(rawUseCase);
    if (keys.length !== 1) {
      throw new YAMLSpecError('Object must be in the form:'
                              + '{name: {Usecase data}'
                              + `Got the following: \n${rawUseCase}`);
    }

    [this.name] = keys;
    const useCaseAttributes = rawUseCase[this.name];
    if (useCaseAttributes.type !== 'use case') {
      throw new YAMLSpecError('Usecase constructor received an'
                              + 'invalid entity type. Received "'
                              + `${useCaseAttributes.type}`
                              + '" expected "use case"');
    }

    Object.keys(rawUseCase[this.name]).forEach((key) => {
      switch (key) {
        case 'description': {
          this.description = rawUseCase[this.name].description;
          break;
        }
        case 'path': {
          if (
            _.isArray(useCaseAttributes.path)
            && useCaseAttributes.path.every((elem) => _.isString(elem))
          ) {
            this.path = useCaseAttributes.path;
          } else {
            throw new YAMLSpecError(`The Use Case "${this.name}" contains an invalid path`);
          }
          break;
        }
        case 'metadata': {
          this.metadata = rawUseCase[this.name].metadata;
          break;
        }
        case 'type': {
          break;
        }
        default: {
          throw new YAMLSpecError(`Use case: ${this.name} has an invalid key: ${key}`);
        }
      }
    });

    if (rawUseCase[this.name].path == null) {
      throw new YAMLSpecError(`The Use Case "${this.name}" contains an invalid path`);
    }
  }
}
